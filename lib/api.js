const Promise = require("bluebird");
const rp = require("request-promise");
const {
  Bookmaker,
  Country,
  Match,
  Team,
  Tournament
} = require("./model.js");
const {
  MatchHistory,
  MatchSnapshot
} = require("./data.js");

class Api {

  constructor() {
    this.root = "https://ec2-52-56-215-130.eu-west-2.compute.amazonaws.com:8443/rest/";
    this.requestOptions = {strictSSL: false, json: false};
  }

  loadBookmakers() {
    return rp(this.root + "bookmakers", this.requestOptions)
      .then(str => JSON.parse(str))
      .then(xs => xs.map((x) => new Bookmaker(x)));
  }

  loadCountries() {
    return rp(this.root + "countries", this.requestOptions)
      .then(str => JSON.parse(str))
      .then(xs => xs.map(x => new Country(x)));
  }

  loadTeams(countryId) {
    return rp(`${this.root}countries/${countryId}/teams`, this.requestOptions)
      .then(str => JSON.parse(str))
      .then(xs => xs.map(x => new Team(x)));
  }

  loadTournaments() {
    return rp(this.root + "tournaments", this.requestOptions)
      .then(str => JSON.parse(str))
      .then(xs => xs.map(x => new Tournament(x)));
  }

  loadTournamentSnapshot(tournamentId) {
    return rp(`${this.root}tournaments/${tournamentId}/matches/open`, this.requestOptions)
      .then(str => JSON.parse(str))
      .then(xs => xs.map(x => new MatchSnapshot(x)));
  }

  loadOpenMatchHistory(tournamentId, matchId) {
    return rp(`${this.root}tournaments/${tournamentId}/matches/open/${matchId}`, this.requestOptions)
      .then(str => JSON.parse(str))
      .then(x => new MatchHistory(x));
  }

  *makeHistoryGenerator(tournamentId, count, from = Math.pow(2, 62)) {
    let urlPromise = new Promise((resolve) => {
      resolve(`${this.root}tournaments/${tournamentId}/matches/history?from=${from}&count=${count}`);
    });
    while (true) {
      const promise = urlPromise
        .then(url => rp(url, this.requestOptions))
        .then((str) => JSON.parse(str))
        .then((xs) => xs.map((x) => new Match(x)));
      yield promise;
      urlPromise = promise.then((xs) => {
        if (xs.length == 0) {
          throw new Error("done");
        } else {
          const newFrom = xs[xs.length - 1].getStart();
          return `${this.root}tournaments/${tournamentId}/matches/history?from=${newFrom}&count=${count}`;
        }
      });
    }
  }

  loadTournamentHistoryWhile(cond, tournamentId, count, from) {
    function loadAll(g, allMatches) {
      return g.next().value.then((xs) => {
        const newAllMatches = allMatches.concat(xs);
        if (cond(newAllMatches)) {
          return loadAll(g, newAllMatches);
        } else {
          return newAllMatches;
        }
      }, (err) => {
        if (err.message != "done") {
          throw err;
        } else {
          return allMatches;
        }
      });
    }

    return loadAll(
      this.makeHistoryGenerator(tournamentId, count, from),
      []
    );
  }

  loadMatchHistory(tournamentId, matchId) {
    return rp(`${this.root}tournaments/${tournamentId}/matches/history/${matchId}`, this.requestOptions)
      .then(str => JSON.parse(str))
      .then(x => new MatchHistory(x));
  }

}

exports.Api = Api;
