const {Api} = require("./api.js");

const api = new Api();

/*
api.loadBookmakers()
  .then(console.log);
*/

/*
api.loadCountries()
  .then(console.log);
*/

/*
api.loadTournaments()
  .then(console.log);
*/

/*
api.loadTeams(5)
  .then(console.log);
*/

/*
api.loadTournamentSnapshot(81)
  .then(console.log);
*/

/*
api.loadOpenMatchHistory(81, 496)
  .then(console.log);
*/

/*
const g = api.makeHistoryGenerator(81, 5);
g.next().value.then((xs) => {
  console.log(xs);
  return g.next().value;
}).then((xs) => {
  console.log(xs);
});
*/

/*
api.loadTournamentHistoryWhile(xs => xs.length < 200, 81, 5)
  .then(console.log);
*/

/*
api.loadMatchHistory(81, 41)
  .then(console.log);
*/

