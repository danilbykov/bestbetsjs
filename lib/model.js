const format = require("date-format");

class Bookmaker {
  constructor(json) {
    this._id = json.id;
    this._name = json.name;
  }

  getId() {return this._id;}
  getName() {return this._name;}
}

class Country {
  constructor(json) {
    this._id = json.id;
    this._name = json.name;
  }

  getId() {return this._id;}
  getName() {return this._name;}
}

class Team {
  constructor(json) {
    this._id = json.id;
    this._name = json.name;
  }

  getId() {return this._id;}
  getName() {return this._name;}
}

class Tournament {
  constructor(json) {
    this._id = json.id;
    this._name = json.name;
    this._country = json.country;
  }

  getId() {return this._id;}
  getName() {return this._name;}
  getCountry() {return this._country;}
}

class Match {
  constructor(json) {
    this._id = json.id;
    this._homeTeamId = json.homeTeamId;
    this._awayTeamId = json.awayTeamId;
    this._start = json.start;
    this._tournamentId = json.tournamentId;
  }

  getId() {return this._id;}
  getHomeTeamId() {return this._homeTeamId;}
  getAwayTeamId() {return this._awayTeamId;}
  getStart() {return this._start;}
  getTournamentId() {return this._tournamentId;}

  inspect() {
    return `<Match id=${this._id} name=${this._homeTeamId} vs ${this._awayTeamId}`
      + ` start=${format('MM-dd hh:mm', new Date(this._start))}`
      + ` tournament=${this._tournamentId} />`;
  }
}

exports.Bookmaker = Bookmaker;
exports.Country = Country;
exports.Team = Team;
exports.Tournament = Tournament;
exports.Match = Match;
