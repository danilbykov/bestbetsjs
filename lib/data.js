const format = require("date-format");
const {Match} = require("./model.js");

class TwoLineBets {
  constructor(json) {
    this._homeWin = json['homeWin'];
    this._awayWin = json['awayWin'];
  }

  getHomeWin() {return this._homeWin;}
  getAwayWin() {return this._awayWin;}

  toString() {
    return this.inspect();
  }
  inspect() {
    return `(1=${this._homeWin.toFixed(3)} 2=${this._awayWin.toFixed(3)})`;
  }
}

class ThreeLineBets {
  constructor(json) {
    this._homeWin = json['homeWin'];
    this._draw = json['draw'];
    this._awayWin = json['awayWin'];
  }

  getHomeWin() {return this._homeWin;}
  getDraw() {return this._draw;}
  getAwayWin() {return this._awayWin;}

  toString() {
    return this.inspect();
  }
  inspect() {
    return `(1=${this._homeWin.toFixed(3)} x=${this._draw.toFixed(3)} 2=${this._awayWin.toFixed(3)})`;
  }
}

class BookmakerSnapshot {
  constructor(json) {
    this._homeWin = json['homeWin'];
    this._draw = json['draw'];
    this._awayWin = json['awayWin'];
    this._notHomeWin = json['notHomeWin'];
    this._notDraw = json['notDraw'];
    this._notAwayWin = json['notAwayWin'];
    this._drawNoBetHome = json['drawNoBetHome'];
    this._drawNoBetAway = json['drawNoBetAway'];
    this._bothTeamsScore = json['bothTeamsScore'];
    this._bothTeamsNotScore = json['bothTeamsNotScore'];
    this._oddGoals = json['oddGoals'];
    this._evenGoals = json['evenGoals'];
    this._correctScores = json['correctScores'];
    this._overTotals = json['overTotals'];
    this._underTotals = json['underTotals'];
    this._asianHandicaps = {};
    this._europeanHandicaps = {};
    for (let key in json['asianHandicaps']) {
      this._asianHandicaps[key] = new TwoLineBets(json.asianHandicaps[key]);
    }
    for (let key in json['europeanHandicaps']) {
      this._europeanHandicaps[key] = new ThreeLineBets(json.europeanHandicaps[key]);
    }
  }

  getHomeWin() {return this._homeWin;}
  getDraw() {return this._draw;}
  getAwayWin() {return this._awayWin;}
  getNotHomeWin() {return this._notHomeWin;}
  getNotDraw() {return this._notDraw;}
  getNotAwayWin() {return this._notAwayWin;}
  getDrawNoBetHome() {return this._drawNoBetHome;}
  getDrawNoBetAway() {return this._drawNoBetAway;}
  getBothTeamsScore() {return this._bothTeamsScore;}
  getBothTeamsNotScore() {return this._bothTeamsNotScore;}
  getOddGoals() {return this._oddGoals;}
  getEvenGoals() {return this._evenGoals;}
  getCorrectScores() {return this._correctScores;}
  getOverTotals() {return this._overTotals;}
  getUnderTotals() {return this._underTotals;}
  getAsianHandicaps() {return this._asianHandicaps;}
  getEuropeanHandicaps() {return this._europeanHandicaps;}

  inspect() {
    function dumpMap(m) {
      const arr = ["["];
      for (let key in m) {
        arr.push(key);
        arr.push("->");
        arr.push(m[key].toFixed(3));
        arr.push(" ");
      }
      arr.push("]");
      return arr.join("");
    }
    function dumpMap2(m) {
      const arr = ["["];
      for (let key in m) {
        arr.push(key);
        arr.push("->");
        arr.push(m[key]);
        arr.push(" ");
      }
      arr.push("]");
      return arr.join("");
    }
    return `<BookmakerSnapshot 1=${this._homeWin.toFixed(3)} x=${this._draw.toFixed(3)} 2=${this._awayWin.toFixed(3)}`
      + ` p1=${this._drawNoBetHome.toFixed(3)} p2=${this._drawNoBetAway.toFixed(3)}`
      + ` 1x=${this._notAwayWin.toFixed(3)} 12=${this._notDraw.toFixed(3)} 2x=${this._notHomeWin.toFixed(3)}`
      + ` score=${this._bothTeamsScore.toFixed(3)} notscore=${this._bothTeamsNotScore.toFixed(3)}`
      + ` odd=${this._oddGoals.toFixed(3)} even=${this._evenGoals.toFixed(3)}`
      + ` scores=${dumpMap(this._correctScores)}`
      + ` overtotals=${dumpMap(this._overTotals)} undertotals=${dumpMap(this._underTotals)}`
      + ` asianHandicaps=${dumpMap2(this._asianHandicaps)}`
      + ` europeanHandicaps=${dumpMap2(this._europeanHandicaps)} />`;
  }
}

class HistoricalBet {

  constructor(json) {
    this._time = json['time']
    this._value = json['value']
  }

  getTime() {
    return this._time;
  }
  getValue() {
    return this._value;
  }

  toString() {
    return this.inspect();
  }
  inspect() {
    return `(${this._value.toFixed(3)} ${format('MM-dd hh:mm', new Date(this._time))})`;
  }
}

class TwoLineHistory {
  constructor(json) {
    this._homeWin = json['homeWin'].map(x => new HistoricalBet(x));
    this._awayWin = json['awayWin'].map(x => new HistoricalBet(x));
  }

  getHomeWin() {return this._homeWin;}
  getAwayWin() {return this._awayWin;}

  toString() {
    return this.inspect();
  }
  inspect() {
    return `(1=${this._homeWin} 2=${this._awayWin})`;
  }
}

class ThreeLineHistory {
  constructor(json) {
    this._homeWin = json['homeWin'].map(x => new HistoricalBet(x));
    this._draw = json['draw'].map(x => new HistoricalBet(x));
    this._awayWin = json['awayWin'].map(x => new HistoricalBet(x));
  }

  getHomeWin() {return this._homeWin;}
  getDraw() {return this._draw;}
  getAwayWin() {return this._awayWin;}

  toString() {
    return this.inspect();
  }
  inspect() {
    return `(1=${this._homeWin} x=${this._draw} 2=${this._awayWin})`;
  }
}

class BookmakerHistory {
  constructor(json) {
    this._homeWin = json['homeWin'].map(x => new HistoricalBet(x));
    this._draw = json['draw'].map(x => new HistoricalBet(x));
    this._awayWin = json['awayWin'].map(x => new HistoricalBet(x));
    this._notHomeWin = json['notHomeWin'].map(x => new HistoricalBet(x));
    this._notDraw = json['notDraw'].map(x => new HistoricalBet(x));
    this._notAwayWin = json['notAwayWin'].map(x => new HistoricalBet(x));
    this._drawNoBetHome = json['drawNoBetHome'].map(x => new HistoricalBet(x));
    this._drawNoBetAway = json['drawNoBetAway'].map(x => new HistoricalBet(x));
    this._bothTeamsScore = json['bothTeamsScore'].map(x => new HistoricalBet(x));
    this._bothTeamsNotScore = json['bothTeamsNotScore'].map(x => new HistoricalBet(x));
    this._oddGoals = json['oddGoals'].map(x => new HistoricalBet(x));
    this._evenGoals = json['evenGoals'].map(x => new HistoricalBet(x));
    this._correctScores = {};
    this._overTotals = {};
    this._underTotals = {};
    for (let key in json.correctScores) {
      this._correctScores[key] = json.correctScores[key].map(x => new HistoricalBet(x));
    }
    for (let key in json.overTotals) {
      this._overTotals[key] = json.overTotals[key].map(x => new HistoricalBet(x));
    }
    for (let key in json.underTotals) {
      this._underTotals[key] = json.underTotals[key].map(x => new HistoricalBet(x));
    }
    this._asianHandicaps = {};
    this._europeanHandicaps = {};
    for (let key in json['asianHandicaps']) {
      this._asianHandicaps[key] = new TwoLineHistory(json.asianHandicaps[key]);
    }
    for (let key in json['europeanHandicaps']) {
      this._europeanHandicaps[key] = new ThreeLineHistory(json.europeanHandicaps[key]);
    }
  }

  getHomeWin() {return this._homeWin;}
  getDraw() {return this._draw;}
  getAwayWin() {return this._awayWin;}
  getNotHomeWin() {return this._notHomeWin;}
  getNotDraw() {return this._notDraw;}
  getNotAwayWin() {return this._notAwayWin;}
  getDrawNoBetHome() {return this._drawNoBetHome;}
  getDrawNoBetAway() {return this._drawNoBetAway;}
  getBothTeamsScore() {return this._bothTeamsScore;}
  getBothTeamsNotScore() {return this._bothTeamsNotScore;}
  getOddGoals() {return this._oddGoals;}
  getEvenGoals() {return this._evenGoals;}
  getCorrectScores() {return this._correctScores;}
  getOverTotals() {return this._overTotals;}
  getUnderTotals() {return this._underTotals;}
  getAsianHandicaps() {return this._asianHandicaps;}
  getEuropeanHandicaps() {return this._europeanHandicaps;}

  inspect() {
    function dumpMap(m) {
      const arr = ["["];
      for (let key in m) {
        arr.push("\n\t");
        arr.push(key);
        arr.push(" -> [");
        arr.push(m[key]);
        arr.push("]");
      }
      arr.push("]");
      return arr.join("");
    }
    return `<BookmakerHistory\n1=[${this._homeWin}]\nx=[${this._draw}]\n2=[${this._awayWin}]`
      + `\np1=[${this._drawNoBetHome}]\np2=[${this._drawNoBetAway}]`
      + `\n1x=[${this._notAwayWin}]\n12=[${this._notDraw}]\n2x=[${this._notHomeWin}]`
      + `\nscore=[${this._bothTeamsScore}]\nnotscore=[${this._bothTeamsNotScore}]`
      + `\nodd=[${this._oddGoals}]\neven=[${this._evenGoals}]`
      + `\nscores=${dumpMap(this._correctScores)}`
      + `\novertotals=${dumpMap(this._overTotals)}\nundertotals=${dumpMap(this._underTotals)}`
      + `\nasianHandicaps=${dumpMap(this._asianHandicaps)}`
      + `\neuropeanHandicaps=${dumpMap(this._europeanHandicaps)} />`;
  }
}

class MatchSnapshot {

  constructor(json) {
    this._match = new Match(json.match);
    this._bookmakers = {};
    for (var key in json.bookmakers) {
      this._bookmakers[key] = new BookmakerSnapshot(json.bookmakers[key]);
    }
  }

  getMatch() {
    return this._match;
  }

  getBookmakers() {
    return this._bookmakers;
  }

  getSnapshot(bookmaker) {
    return this._bookmakers[bookmaker];
  }
}

class MatchHistory {

  constructor(json) {
    this._match = new Match(json.match);
    this._bookmakers = {};
    for (var key in json.bookmakers) {
      this._bookmakers[key] = new BookmakerHistory(json.bookmakers[key]);
    }
  }

  getMatch() {
    return this._match;
  }

  getBookmakers() {
    return this._bookmakers;
  }

  getHistory(bookmaker) {
    return this._bookmakers[bookmaker];
  }
}

exports.MatchSnapshot = MatchSnapshot;
exports.MatchHistory = MatchHistory;
